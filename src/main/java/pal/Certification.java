package pal;

class Certification {

    static String groupCertificatesCycles(Graph[] graphs, int cycleSize) {
        MyCountMap<String, String> tupleCountSet = new MyCountMap<>();

        for (Graph graph : graphs) {
            String certificate = graph.getBasicCertificate();
            String cycleCert = graph.getCycleHash(cycleSize);
            tupleCountSet.putSpecial(certificate, cycleCert);
        }
//        System.out.println(tupleCountSet.toString());
        return tupleCountSet.printSet();
    }
}
