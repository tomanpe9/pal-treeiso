package pal;

import java.util.*;

class MyCountMap<K, V> extends HashMap<K, HashMap<V, Integer>> {

    void putSpecial(K key, V value) {
        HashMap<V, Integer> valueMap = get(key);
        if (valueMap != null) {
            Integer valueCount = valueMap.get(value);
            if (valueCount != null) {
                valueMap.put(value, ++valueCount);
            } else {
                valueMap.put(value, 1);
            }
        } else {
            valueMap = new HashMap<>();
            valueMap.put(value, 1);
            put(key, valueMap);
        }
    }

    String printSet() {
        StringBuilder builder = new StringBuilder();
        List<Integer> sizes = new ArrayList<>();
        for (HashMap<V, Integer> value : values()) {
            sizes.addAll(value.values());
        }
        Collections.sort(sizes);
        for (Integer size : sizes) {
            builder.append(" ")
                    .append(size);
        }
        builder.deleteCharAt(0);
        return builder.toString();
    }

    public String toString() {
        Iterator<Entry<K,HashMap<V, Integer>>> i = entrySet().iterator();
        if (! i.hasNext())
            return "{}";

        StringBuilder sb = new StringBuilder();
//        sb.append('{');
        for (;;) {
            Entry<K,HashMap<V, Integer>> e = i.next();
            K key = e.getKey();
            HashMap<V, Integer> value = e.getValue();
            sb.append(key);
            sb.append('=');
            sb.append(toStringMap(value));
            if (! i.hasNext())
                return sb.toString();
//              return sb.append('}').toString();
            sb.append('\n');
        }
    }

    public String toStringMap(HashMap<V, Integer> map) {
        Iterator<Entry<V, Integer>> i = map.entrySet().iterator();
        if (! i.hasNext())
            return "{}";

        StringBuilder sb = new StringBuilder();
//        sb.append('{');
        for (;;) {
            Entry<V, Integer> e = i.next();
            V key = e.getKey();
            Integer value = e.getValue();
            sb.append('\n');
            sb.append('\t');
            sb.append(key);
            sb.append('=');
            sb.append(value);
            if (! i.hasNext())
                return sb.toString();
//              return sb.append('}').toString();
        }
    }

}
