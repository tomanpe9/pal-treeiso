package pal;

import java.util.ArrayList;
import java.util.HashMap;

public class MyHashMultimap<K, V> extends HashMap<K, ArrayList<V>> {

    public int count(K key) {
        ArrayList<V> values = get(key);
        return values != null ? values.size() : 0;
    }

    public void putValue(K key, V value) {
        ArrayList<V> values = get(key);
        if (values != null) {
            values.add(value);
        } else {
            values = new ArrayList<>();
            values.add(value);
            put(key, values);
        }
    }

}

