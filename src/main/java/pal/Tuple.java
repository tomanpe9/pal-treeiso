package pal;

public class Tuple<F, S> {
    private final F first;
    private final S second;

    private Tuple(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public F getFirst() {
        return first;
    }

    public S getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return "Tuple(" + first + ", " + second + ')';
    }

    static class StringTuple extends Tuple<String, String> {
        StringTuple(String first, String second) {
            super(first, second);
        }

        @Override
        public int hashCode() {
            return getFirst().hashCode() * 31 * getSecond().hashCode();
        }
    }
}
