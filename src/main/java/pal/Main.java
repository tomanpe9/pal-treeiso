package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

    private static final String DELIMITER = " ";

    private static int cycleSize;

    /**
     * Running algorithm inside.
     * <p>
     * Reading from System.in and returning solution in System.out
     *
     * @throws IOException on fail to read
     */
    public static void main(String[] args) throws IOException {
        String solution = processSolution(new BufferedReader(new InputStreamReader(System.in)));
        System.out.println(solution);
    }

    /**
     * Moved to new method to allow adding different BufferedReader
     *
     * @param bufferedReader with graph data
     * @return best solution
     * @throws IOException on fail to read
     */
    static String processSolution(BufferedReader bufferedReader) throws IOException {
        Graph[] graphs = prepareGraphs(bufferedReader);
        return Certification.groupCertificatesCycles(graphs, cycleSize);
    }
    // region Data reading methods

    /**
     * Vertex and Blues data
     *
     * @param bufferedReader with graph data
     * @return vertexes array
     * @throws IOException on fail to read
     */
    private static Graph[] prepareGraphs(BufferedReader bufferedReader) throws IOException {
        String[] sizes = splitString(bufferedReader.readLine(), DELIMITER);

        int graphsNo = Integer.parseInt(sizes[0]);
        int verticesNo = Integer.parseInt(sizes[1]);
        int edgesNo = Integer.parseInt(sizes[2]);
        // Tree is acyclic when (edgesNo = verticesNo - 1)
        cycleSize = edgesNo - (verticesNo - 1);

        // region Prepare Graphs
        Graph[] graphs = new Graph[graphsNo];
        for (int graphId = 0; graphId < graphsNo; graphId++) {
            graphs[graphId] = new Graph(verticesNo);
        }
        // endregion
        for (int graphId = 0; graphId < graphsNo; graphId++) {
            Graph graph = graphs[graphId];
            for (int i = 0; i < edgesNo; i++) {
                String[] edgeArray = splitString(bufferedReader.readLine(), DELIMITER);

                int outboundId = Integer.parseInt(edgeArray[0]) - 1;
                int inboundId = Integer.parseInt(edgeArray[1]) - 1;

                graph.addNewEdge(outboundId, inboundId);
            }
            graphs[graphId] = graph;
        }
        return graphs;
    }


    /**
     * BRUTALLY FASTER THEN String.split()
     * <p>
     * Taken from sun - StringUtils
     */
    private static String[] splitString(String line, String delimiter) {
        StringTokenizer tokenizer = new StringTokenizer(line, delimiter);
        String[] splitString = new String[tokenizer.countTokens()];

        for (int var4 = 0; var4 < splitString.length; ++var4) {
            splitString[var4] = tokenizer.nextToken();
        }

        return splitString;
    }
    //endregion

}

