package pal;

import java.util.ArrayList;
import java.util.Collections;

public class Vertex implements Comparable<Vertex> {

    final int id;
    private boolean flag;

    private int childNo;

    Vertex parent;
    Vertex cycle;

    private ArrayList<Vertex> certificates = new ArrayList<>();
    private String certificate;

    private ArrayList<Integer> cycles = new ArrayList<>();

    Vertex(int id) {
        this.id = id;
    }

    //region Filling data
    void addNewParent(Vertex newInDegree) {
        if (parent == null) {
            parent = newInDegree;
        } else {
            cycle = newInDegree;
            flag = true;
            cycles.add(id);
            --childNo;
        }
    }

    void addChild() {
        this.childNo++;
    }

    void switchParents() {
        Vertex replaceVertex = parent;
        parent = cycle;
        cycle = replaceVertex;
    }
    // endregion

    // region Certificate
    void addCertificateChild(Vertex child) {
        --childNo;
        certificates.add(child);
    }

    String getCertificate(Graph graph) {
        if (certificate == null) {
            certify(graph);
        }
        return certificate;
    }

    void certify(Graph graph) {
        Collections.sort(certificates);
        StringBuilder cert = new StringBuilder();
        cert.append('0'); // '2' could be creating problems?
//        cert.append((isCyclic() ? '2' : '0'));
        int length = 1;
        String prevCert = null;
        for (Vertex child : certificates) {
            cert.append(child.certificate);
//            int same = child.certificate.equals(prevCert) ?
//                    child.certificate.length() : 0;
            length -= child.certificate.equals(prevCert) ?
                    child.certificate.length() : 0;

            for (Integer cycleVertex : child.cycles) {
                int cycleCert = graph.cycleMap.get(cycleVertex);
//                graph.cycleMap.put(cycleVertex, cycleCert + length - same);
                graph.cycleMap.put(cycleVertex, cycleCert + length);
            }
            cycles.addAll(child.cycles);

            prevCert = child.certificate;
            length += prevCert.length();
        }
        cert.append((isCyclic() ? '3' : '1'));
        certificate = cert.toString();
    }
    // endregion

    // region Basic methods
    boolean isCyclic() {
        return flag;
    }

    boolean isRoot() {
        return parent == null;
    }

    boolean isLeaf() {
        return childNo == 0;
    }

    @Override
    public String toString() {
        return "Vertex(" + id + ')'
                + (isCyclic() ? " is cyclic" : "")
                + (isLeaf() ? " is leaf" : "")
                + (isRoot() ? " is root" : "")
                + "; out=" + childNo;
    }

    @Override
    public int compareTo(Vertex o) {
        return this.certificate.compareTo(o.certificate);
    }
    // endregion
}
