package pal;

import java.util.*;

class Graph {

    private final Vertex[] vertices;
    HashMap<Integer, Integer> cycleMap = new HashMap<>();
    private Vertex cycleRoot;
    private Vertex root;

    Graph(int verticesNo) {
        vertices = new Vertex[verticesNo];
        for (int i = 0; i < verticesNo; i++) {
            vertices[i] = new Vertex(i + 1);
        }
    }

    void addNewEdge(int outboundId, int inboundId) {
        Vertex inboundVertex = vertices[inboundId];
        Vertex outboundVertex = vertices[outboundId];

        inboundVertex.addNewParent(outboundVertex);
        outboundVertex.addChild();
    }

    private ArrayList<Vertex> collate() {
        ArrayList<Vertex> leafs = new ArrayList<>();
        for (Vertex vertex : vertices) {
            if (vertex.isLeaf()) {
                leafs.add(vertex);
            } else if (vertex.isRoot()) {
                root = vertex;
            }
            if (vertex.isCyclic()) {
                if (vertex.parent.isCyclic()) {
                    vertex.switchParents();
                }
                cycleMap.put(vertex.id, 0);
                cycleRoot = vertex;
            }
        }
        return leafs;
    }

    String getCompleteCertificate(int cycleSize) {
        String certificate = getBasicCertificate();
        String cycleCert = getCycleHash(cycleSize);
        System.out.println(certificate + " " + cycleCert);
        return certificate + " " + cycleCert;
    }

    String getBasicCertificate() {
        ArrayList<Vertex> leafs = collate();
        while (!leafs.isEmpty()) {
            ArrayList<Vertex> newLeafs = new ArrayList<>();
            for (Vertex leaf : leafs) {
                leaf.certify(this);
                if (leaf.parent != null) {
                    leaf.parent.addCertificateChild(leaf);
                    if (leaf.parent.isLeaf()) {
                        newLeafs.add(leaf.parent);
                    }
                }
            }
            leafs = newLeafs;
        }
        return root.getCertificate(this);
    }

    String getCycleHash(int cycleSize) {
        String[] links = new String[cycleSize];
        Vertex previous = cycleRoot;
        for (int i = 0; i < cycleSize; i++) {
            Vertex next = previous.cycle;
            String delimiter = "-"; // if not enough => add length between
            links[i] = cycleMap.get(previous.id) + delimiter + cycleMap.get(next.id);
            previous = next;
        }
        Arrays.sort(links);
        return Arrays.toString(links);
    }

    Vertex[] getVertices() {
        return vertices;
    }

    @Override
    public String toString() {
        return "G(" + cycleMap.keySet().toString() + ')';
    }
}
