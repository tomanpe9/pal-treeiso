package pal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class MyCountSet<K> extends HashMap<K, Integer> {

	void put(K key) {
		Integer value = get(key);
		if (value != null) {
			put(key, ++value);
		} else {
			put(key, 1);
		}
	}

	String printSet() {
		StringBuilder builder = new StringBuilder();
		List<Integer> sizes = new ArrayList<>(values());
		Collections.sort(sizes);
		for (Integer size : sizes) {
			builder.append(" ")
					.append(size);
		}
		builder.deleteCharAt(0);
		return builder.toString();
	}

}
